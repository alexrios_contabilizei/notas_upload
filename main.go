package main

import (
	"bytes"
	"cloud.google.com/go/storage"
	"encoding/xml"
	"fmt"
	"golang.org/x/net/context"
	"google.golang.org/api/option"
	"io"
	"log"
	"net/http"
	"strings"
	"time"
)

const maxUploadSize = 500 * 1024 // 500 kb

func handler(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	cnpj := r.Header.Get("cnpj")
	//nfType := r.Header.Get("type")
	fmt.Println(cnpj)

	//Valida tamanho do arquivo
	fmt.Println("validating file size")
	r.Body = http.MaxBytesReader(w, r.Body, maxUploadSize)
	if err := r.ParseMultipartForm(maxUploadSize); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("FILE_IS_TOO_BIG"))
		return
	}
	fmt.Println("reading file")
	//le arquivo do form
	file, fileHeader, err := r.FormFile("file")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	fmt.Println("validate extension")
	//valida extensao
	if !strings.HasSuffix(fileHeader.Filename, ".xml") {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("FILE_IS_NOT_XML"))
		return
	}

	//
	// Read bufer
	//

	buf := bytes.NewBuffer(nil)
	if _, err := io.Copy(buf, file); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	bytesRead := buf.Bytes()
	fmt.Printf("%v bytes from file read \n", len(bytesRead))

	//
	// PARSE XML to Model
	//
	fmt.Println("parsing")
	err, nf := ToNFSaida(bytesRead)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	//
	// Validate model
	//
	fmt.Println("Validate model")
	err = nf.Validate()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	//
	//  G C P
	//

	fmt.Println("Client GCP")
	ctx := context.Background()
	client, err := storage.NewClient(ctx, option.WithCredentialsFile("/home/alexrios/Workspace/upload-labs/notas/linx-sandbox-recebimento-nf.json"))
	if err != nil {
		fmt.Println(err)
		return
	}
	bkt := client.Bucket("notas_fiscais_storage_bucket")
	obj := bkt.Object(fileHeader.Filename)
	objWriter := obj.NewWriter(ctx)

	byteWrote, err := objWriter.Write(bytesRead)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("%v transfered \n", byteWrote)
	if err := objWriter.Close(); err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Done GCP Write")
	t := time.Now()
	elapsed := t.Sub(start)
	fmt.Println(elapsed)
}

func main() {
	http.HandleFunc("/upload", handler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func ToNFSaida(data []byte) (e error, saida NotaFiscalSaida) {
	var nfeProc NotaFiscalSaida
	err := xml.Unmarshal(data, &nfeProc)
	if err != nil {
		return err, NotaFiscalSaida{}
	}
	return nil, nfeProc
}

type NotaFiscalSaida struct {
	XMLName xml.Name `xml:"nfeProc"`
	Versao  string   `xml:"versao,attr"`
	NFe     struct {
		InfNFe struct {
			ID     string `xml:"Id,attr"`
			Versao string `xml:"versao,attr"`
			Ide    struct {
				DhEmi string `xml:"dhEmi"`
			} `xml:"ide"`
		} `xml:"infNFe"`
	} `xml:"NFe"`
}

func (nf *NotaFiscalSaida) Validate() error {
	if _, err := time.Parse(time.RFC3339, nf.NFe.InfNFe.Ide.DhEmi); err != nil {
		return err
	}
	return nil
}
